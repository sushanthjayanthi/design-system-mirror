import React from 'react';

import { Format } from '../../src';

export default function Example() {
  return <Format max={10}>{20}</Format>;
}
