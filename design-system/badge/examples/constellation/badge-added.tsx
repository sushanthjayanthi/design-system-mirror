import React from 'react';

import Badge from '../../src';

export default function Example() {
  return <Badge appearance="added">+100</Badge>;
}
